import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
import random
import math
import operator

IDEAS_COUNT = 125250
GENERATOR_MIN_STAKE = 100
GENERATOR_MAX_STAKE = 1000
VOTER_MIN_STAKE = 100
VOTER_MAX_STAKE = 1000
VOTERS_COUNT = 10
INFLATION_RATE = 1.5
QUANTILES = 500

def genIdeas():
  ideas = []
  for i in range(1, IDEAS_COUNT + 1):
    idea = {
      'userId': i,
      'generatorToken': random.randint(GENERATOR_MIN_STAKE, GENERATOR_MAX_STAKE),
      'voterToken': random.randint(VOTER_MIN_STAKE, VOTER_MAX_STAKE),
      'voters': random.randint(1, VOTERS_COUNT)
    }
    if idea['voters'] == 0:
      idea['voterToken'] = 0
    ideas.append(idea)
  return ideas

def computeIdeaScore(ideas):
  scoredIdeas = []
  for idea in ideas:
    idea['score'] = idea['generatorToken'] + idea['voterToken'] * idea['voters'] * INFLATION_RATE
    idea['score'] = int(idea['score'])
    scoredIdeas.append(idea)
  return scoredIdeas

def computeLinearAllocation(ideas):
  allocatedIdeas = []
  # sort ideas by best score
  ideas.sort(key=operator.itemgetter('score'), reverse=True)
  # cut the quantile
  ideas = ideas[:QUANTILES]
  # calculate ranks
  for i in range(1, len(ideas) + 1):
    ideas[i - 1]['rank'] = i
  # calculate allocation
  for idea in ideas:
    inverseRank = len(ideas) - idea['rank'] + 1
    # allocation = format(inverseRank / IDEAS_COUNT, '.2f')
    allocation = inverseRank / IDEAS_COUNT
    idea['allocation'] = allocation
    allocatedIdeas.append(idea)
  return ideas

def computeLogAllocation(ideas):
  allocatedIdeas = []
  # sort ideas by best score
  ideas.sort(key=operator.itemgetter('score'), reverse=True)
  # cut the quantile
  ideas = ideas[:QUANTILES]
  # calculate ranks
  for i in range(1, len(ideas) + 1):
    ideas[i - 1]['rank'] = i
  # calculate allocation
  for idea in ideas:
    inverseRank = len(ideas) - idea['rank'] + 1
    allocation = math.log(inverseRank, 2)
    idea['allocation'] = allocation
    allocatedIdeas.append(idea)
  return ideas

def convertToPlot(ideas):
  coords = []
  for idea in ideas:
    coords.append(float(idea['allocation']))
  return coords;


if __name__ == "__main__":
  scoredIdeas = computeIdeaScore(genIdeas())
  # allocatedIdeas = computeLinearAllocation(scoredIdeas)
  allocatedIdeas = computeLogAllocation(scoredIdeas)
  coordinates = convertToPlot(allocatedIdeas)
  plt.ylabel('% allocation')
  plt.xlabel('nº ideas')
  plt.plot(coordinates, label='allocation')  
  plt.legend()
  plt.show()